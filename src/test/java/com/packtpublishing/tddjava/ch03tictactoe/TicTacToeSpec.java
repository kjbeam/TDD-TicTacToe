package com.packtpublishing.tddjava.ch03tictactoe;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class defines unit tests for a Tic-Tac-Toe game.
 *
 * @author kbeam Date:    8/21/2018 Time:    10:53 AM
 */
public class TicTacToeSpec {

  private TicTacToe ticTacToe;

  @BeforeEach
  final void before() {
    ticTacToe = new TicTacToe();
  }

  @Test
  @DisplayName("X Outside of Board - Runtime Exception")
  void whenXOutsideBoardThenRuntimeException() {
    assertThatExceptionOfType(RuntimeException.class)
        .isThrownBy(() -> ticTacToe.play(5, 2));
  }

  @Test
  @DisplayName("Y Outside of Board - Runtime Exception")
  void whenYOutsideBoardThenRuntimeException() {
    assertThatExceptionOfType(RuntimeException.class)
        .isThrownBy(() -> ticTacToe.play(2, 5));
  }

  @Test
  @DisplayName("Spot is occupied - Runtime Exception")
  void whenOccupiedThenRuntimeException() {
    assertThatExceptionOfType(RuntimeException.class)
        .isThrownBy(() -> {
          ticTacToe.play(2, 1);
          ticTacToe.play(2, 1);
        });
  }

  @Test
  @DisplayName("X should be the first player")
  void firstTurnPlayerShouldBeNext() {
    assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
  }

  @Test
  @DisplayName("O should be after X plays")
  void afterXThenOShouldBeNext() {
    ticTacToe.play(1, 1);
    assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
  }

  @Test
  @DisplayName("By default there is no winner")
  public void whenPlayThenNoWinner() {
    String actual = ticTacToe.play(1,1);
    assertThat(actual).isEqualTo("No Winner");
  }

  @Test
  @DisplayName("Winning Condition 1") //Horizontal
  public void whenPlayAndWholeHorizontalLineThenWiner() {
    ticTacToe.play(1,1); // X
    ticTacToe.play(1,2); // O
    ticTacToe.play(2,1); // X
    ticTacToe.play(2,2); // O
    String actual = ticTacToe.play(3,1); // X
    assertThat(actual).isEqualTo("X is the winner");
  }

}