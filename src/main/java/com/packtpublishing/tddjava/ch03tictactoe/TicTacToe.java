package com.packtpublishing.tddjava.ch03tictactoe;

/**
 * This class defines the methods for a Tic-Tac-Toe game.
 * @author kbeam Date:    8/21/2018 Time:    10:52 AM
 */
class TicTacToe {

  private Character[][] board = {
    {'\0', '\0', '\0'},
    {'\0', '\0', '\0'},
    {'\0', '\0', '\0'}
  };

  private char lastPlayer = '\0';

  private static final int SIZE = 3;

  String play(int x, int y) {
    checkAxis(x);
    checkAxis(y);
    lastPlayer = nextPlayer();
    setBox(x, y, lastPlayer);
    if (isWin()) {
      return lastPlayer + " is the winner";
    }

    return "No Winner";

  }

  void checkAxis(int axis) {
    if (axis > 3 || axis < 1) {
      throw new RuntimeException("X is outside of the board");
    }
  }

  void setBox(int x, int y, char lastPlayer) {
    if (board[x - 1][y - 1] != '\0') {
      throw new RuntimeException("Box is occupied");
    } else {
      board[x - 1][y - 1] = lastPlayer;
    }
  }

  char nextPlayer() {
    if (lastPlayer == 'X') {
      return 'O';
    } else {
      return 'X';
    }
  }

  private boolean isWin() {
    for (int index = 0; index < SIZE; index++) {
      if (board[0][index] + board[1][index] + board[2][index] == lastPlayer * SIZE) {
        return true;
      }
    }
    return false;
  }
}
